#include <iostream>

using namespace std;

int main() {
    int size,input[100], output[100];
    do {
        cout << "enter array size (smaller then 100)" << endl;
        cin >> size;
    } while (size < 1 || size > 100);
    for (int i = 0; i < size; i++) {
        cout << "enter " << i << " item of array" << endl;
        cin >> input[i];
    }
    bool isNormal = true;
    for (int i = 0; i < size - 1 && (isNormal = input[i] * input[i + 1] > 0 || input[i + 1] < 0); i++);
    int counter = 0;
    for (int i = size; i >= 0; i--)
        if ((input[i] % 2)*(input[i] % 2) == 1 == isNormal) {
            output[counter++] = input[i];
        }
    for (int i = 0; i < counter; i++) cout << "output [" << i << "] = " << output[i] << endl;
    system("pause");
    return 0;
}